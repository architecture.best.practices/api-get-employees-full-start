package hg.katas.getApiBack.schedular;

import hg.katas.getApiBack.business.service.EmployeeService;
import hg.katas.getApiBack.persistence.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class EmployeeScheduler {

    @Autowired
    EmployeeService employeeService;

    @Scheduled(fixedDelay = 1000000)
    public void testScheduler(){
        employeeService.getEmployee(PageRequest.of(1,2));
    }
}
