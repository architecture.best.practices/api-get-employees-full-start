package hg.katas.getApiBack.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "employees")
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Employee {

    @Id
    private Integer code;
    private String firstName;
    private String lastName;
    private Integer age;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "code")
    private Desk desk;
}
