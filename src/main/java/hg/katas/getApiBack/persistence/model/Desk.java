package hg.katas.getApiBack.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "desks")
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Desk {

    @Id
    @GeneratedValue
    private Integer code;

    private String name;
}
