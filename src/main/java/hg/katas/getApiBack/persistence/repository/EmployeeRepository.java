package hg.katas.getApiBack.persistence.repository;

import hg.katas.getApiBack.persistence.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, String> {
    @Override
    Page<Employee> findAll(Pageable pageable);
}
