package hg.katas.getApiBack.web.controller;

import hg.katas.getApiBack.business.model.EmployeeData;
import hg.katas.getApiBack.business.service.EmployeeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {

        this.employeeService = employeeService;
    }

    @GetMapping("/employees")// OSIV
    public Page<EmployeeData> getEmployees(@RequestParam String page, @RequestParam String size) {
        Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        final Page<EmployeeData> employees = employeeService.getEmployee(pageable);
        //Appel un service qui prend un peu de temps
        // 4 s
        return employees;
    }
}
