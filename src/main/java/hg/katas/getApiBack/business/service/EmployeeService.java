package hg.katas.getApiBack.business.service;

import hg.katas.getApiBack.business.model.EmployeeData;
import hg.katas.getApiBack.persistence.repository.EmployeeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {

        this.employeeRepository = employeeRepository;
    }

    public Page<EmployeeData> getEmployee(Pageable pageRequest) {
        return employeeRepository.findAll(pageRequest)
                .map(employee -> EmployeeData.builder()
                        .firstName(employee.getFirstName())
                        .lastName(employee.getLastName())
                        .age(employee.getAge())
                        .deskName(employee.getDesk().getName())
                        .build());
    }
}
