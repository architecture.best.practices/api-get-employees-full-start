INSERT INTO desks (code, name) VALUES (1, 'desk name 1');
INSERT INTO desks (code, name) VALUES (2, 'desk name 2');
INSERT INTO desks (code, name) VALUES (3, 'desk name 3');

INSERT INTO employees (code, firstName, lastName, age) VALUES (1, 'firstName1', 'lastName1', 28);
INSERT INTO employees (code, firstName, lastName, age) VALUES (2, 'firstName2', 'lastName2', 23);
INSERT INTO employees (code, firstName, lastName, age) VALUES (3, 'firstName3', 'lastName3', 39);