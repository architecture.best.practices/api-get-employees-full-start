CREATE TABLE employees (
    code INTEGER NOT NULL,
    firstName VARCHAR(100),
    lastName VARCHAR(100),
    age INT,
    PRIMARY KEY (code)
);

CREATE TABLE desks (
    code INTEGER NOT NULL,
    name VARCHAR(100),
    PRIMARY KEY (code)
);