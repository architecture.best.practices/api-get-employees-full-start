package hg.katas.getApiBack.persistence.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "hg.katas.getApiBack.persistence.repository")
@EntityScan(basePackages = "hg.katas.getApiBack.persistence.model")
public class DataBaseConfig {
}
