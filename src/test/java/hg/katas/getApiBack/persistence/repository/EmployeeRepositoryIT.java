package hg.katas.getApiBack.persistence.repository;

import hg.katas.getApiBack.persistence.config.DataBaseConfig;
import hg.katas.getApiBack.persistence.model.Employee;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@DataJpaTest
@ContextConfiguration(classes = DataBaseConfig.class)
class EmployeeRepositoryIT {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    void mustReturnAllEmployees(){
        List<Employee> employees = new ArrayList<>();
        Employee addedEmployee = Employee.builder().build();
        employees.add(addedEmployee);
        employeeRepository.saveAll(employees);
        Page<Employee> employeePage = employeeRepository.findAll(PageRequest.of(0, 2));
        Assertions.assertThat(employeePage).isNotNull();
        Assertions.assertThat(employeePage.getTotalElements()).isEqualTo(1L);
    }

}