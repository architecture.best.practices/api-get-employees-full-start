package hg.katas.getApiBack.web.controller.it.tests;

import hg.katas.getApiBack.TestConfig;
import hg.katas.getApiBack.business.service.EmployeeService;
import hg.katas.getApiBack.persistence.model.Employee;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

@WebMvcTest
@ContextConfiguration(classes = {TestConfig.class, Employee.class})
class EmployeeControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @Test
    void mustReturnValidResponseWhenCallingGetEmployeesApi() throws Exception {
        int page = 0;
        int size = 1;
        String firstNameExample = "FirstNameExample";
        String lastNameExample = "LastNameExample";
        PageRequest pageRequest = PageRequest.of(page, size);
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(Employee.builder()
                .firstName(firstNameExample)
                .lastName(lastNameExample).build());
        Page<Employee> employeePage = PageableExecutionUtils.getPage(employees, pageRequest, () -> 0L);
        Mockito.doReturn(employeePage).when(employeeService).getEmployee(Mockito.any(PageRequest.class));
        mockMvc.perform(MockMvcRequestBuilders.get("/employees")
                .queryParam("page", Integer.toString(page))
                .queryParam("size", Integer.toString(size)))
                .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.content", Matchers.notNullValue()))
        .andExpect(MockMvcResultMatchers.jsonPath("$.content", Matchers.hasSize(1)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.content[0]", Matchers.notNullValue()))
        .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].firstName", Matchers.is(firstNameExample)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].lastName", Matchers.is(lastNameExample)));

    }
}