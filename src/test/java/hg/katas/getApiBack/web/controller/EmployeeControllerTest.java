package hg.katas.getApiBack.web.controller;

import hg.katas.getApiBack.business.model.EmployeeData;
import hg.katas.getApiBack.business.service.EmployeeService;
import hg.katas.getApiBack.persistence.model.Employee;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;

import java.util.ArrayList;

class EmployeeControllerTest {

    @Test
    void mustReturnValidPageableEmployees(){
        EmployeeService employeeService = Mockito.mock(EmployeeService.class);
        ArrayList<EmployeeData> employees = new ArrayList<>();
        employees.add(EmployeeData.builder().build());
        PageRequest expectedPageable = PageRequest.of(0, 3);
        Page<EmployeeData> expectedEmployeeDatapage = PageableExecutionUtils.getPage(employees, expectedPageable, () -> 1L);
        Mockito.doReturn(expectedEmployeeDatapage).when(employeeService).getEmployee(Mockito.any(Pageable.class));
        EmployeeController employeeController = new EmployeeController(employeeService);

        Page<EmployeeData> employeeDatas = employeeController.getEmployees("0", "2");

        Assertions.assertThat(employeeDatas).isNotNull().isEqualTo(expectedEmployeeDatapage);
        ArgumentCaptor<Pageable> pageableArgumentCaptor = ArgumentCaptor.forClass(Pageable.class);
        Mockito.verify(employeeService).getEmployee(pageableArgumentCaptor.capture());
        Assertions.assertThat(pageableArgumentCaptor.getValue()).isNotNull().isEqualTo(expectedPageable);
    }

}