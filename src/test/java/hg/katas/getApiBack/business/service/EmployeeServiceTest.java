package hg.katas.getApiBack.business.service;

import hg.katas.getApiBack.business.model.EmployeeData;
import hg.katas.getApiBack.persistence.model.Employee;
import hg.katas.getApiBack.persistence.repository.EmployeeRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;

import java.util.ArrayList;
import java.util.List;

class EmployeeServiceTest {

    @Test
    void mustReturnValidEmployeeData() {
        EmployeeRepository employeeRepository = Mockito.mock(EmployeeRepository.class);
        Pageable pageRequest = PageRequest.of(1, 8);
        List<Employee> toBeReturnedEmployees = new ArrayList<>();
        toBeReturnedEmployees.add(Employee.builder().build());
        Page<Employee> page = PageableExecutionUtils.getPage(toBeReturnedEmployees, pageRequest, () -> 1L);
        Mockito.doReturn(page).when(employeeRepository).findAll(pageRequest);
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        Page<EmployeeData> employeesDataPage = employeeService.getEmployee(pageRequest);
        Assertions.assertThat(employeesDataPage).isNotNull().hasSize(toBeReturnedEmployees.size());
        Assertions.assertThat(employeesDataPage.getTotalPages()).isNotNull();
    }

}